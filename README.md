
PHP5 - Script
===================

Conteúdo sobre exemplos praticos do **PHP5** aplicado na **Faculdade SENAI FATESG** para o treinamento e Qualificação Profissional, feito em 22 de maio de 2015.

> - Plataforma usada:
>   - Windows/Linux
> - Editor usado
>   - Notepad++
> - Configuração básica do PHP5
>   - Ao iniciar, vai em:
>     - Menu Iniciar > Meu Computador > Botão Direito  >  Propriedades > Configurações avançadas do sistema
>   - "Configurações avançadas do sistema":
>     - Propriedades do sistema > Variáveis de Ambiente > Variáveis de usuário para aluno > Novo…
>     - Nome da variável: PHP
>     - Valor da variável: c:/php/php.exe

## Fundamentos da Programação

> - **Lógica:** Solução de Problemas
> - **Algoritmos:** Problemas Computacionais
> - **Linguagens de Programação:** Tradução de Programas/Execução

![fundamentos-programacao](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/fundamentos-programacao.png)

> **Exemplos:**

> - Linguagem de Máquina *Binário*
> - Lingugaem de Montagem *Assembly*

#### Arquitetura básica de um Site

![exemplo-arquitetura](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/exemplo-arquitetura.png)


#### Como usar o PHP

![uso-php](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/uso-php.png)

#### echo

Escreve texto na Saída (Console)

```php
PHP_EOL # -> Constante para Quebra de Linha;
/n # -> Constante para Quebra de Linha;
. # -> (Operador) Usado para concatenar Texto/Variáveis.
```

#### variável

Local de Armazenamento de Dados

```php
$
```

> **Obs:**
>
> - Armazenar um valor de cada vez (Escalar);
> - Não precisam ser Declaradas.

#### Tipos de dados Escalaveis

- **Integer:** Nº’s Inteiros
- **Float / Double:** Nº’s Reais (Casas Decimais)
- **String:** Texto
- **Boolean:** True / False

#### Tipos Compostos

- **Arrays:**

  ```php
  $x[0],$x[1]
  ```

- **Objetos**

#### Operadores

- **Aritméticos:**

  ```php
  + 	- 	* 	/ 	%
  ```

- **Atribuição:** 

  ```php
  =
  ```

- **Relacionais:** 

  ```php
  < 		> 		== 		=== 	<= 		=> 		!=
  ```

- **Lógicas:** 

  ```php
  & (AND) 	| (OR) 		! (NOT)
  ```

- **Funções (cálculos matemáticos):** 

  ```php
  sin 	cos 	log 	log10
  ```

## Estrutura de condições

Permitem desviar o fluxo de execução do **programa / script** de acordo com o teste de uma condição *pré-determinada*.

![teste-logico](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/teste-logico.png)

#### IF

![if](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/if.png)

#### IF - ELSE

![if-else](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/if-else.png)

#### IF - ELSEIF - ELSE

![if-elseif](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/if-elseif.png)

#### SWITCH

![switch-case](https://gitlab.com/i.leonardo/fac-script-php5/raw/master/img/switch-case.png)

## Licença

> [MIT License](/blob/master/LICENSE)

Copyright © 2019 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)

Manual de instalação oficial do [php.net](http://php.net/manual/en/install.unix.apache2.php).