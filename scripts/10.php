<?php

$senhaSistema = 123456;
$tentativas = 1;

do {
    echo "Digite a senha do Sistema: ";
    $senhaUsuario = fgets(STDIN);

    if ($senhaUsuario == $senhaSistema) {
        echo "ACCESS GRANTED!" . PHP_EOL;
        break; # Parar imediatamente a repetição
    } else {
        echo "ACCESS DENIED!" . PHP_EOL;
        $tentativas++;
    }

} while ($tentativas <= 3);

if ($tentativas > 3) {
    echo "Acesso Bloqueado! Procure o administrador do sistema." . PHP_EOL;
}
