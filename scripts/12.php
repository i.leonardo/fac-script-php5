<?php

echo "Digite a máscara de rede no formato CIDR (/bits): ";
$cidr = fgets(STDIN);
$numero = explode("/", $cidr);
$bitsUm = (int) $numero[1];

$bitsZero = 32 - $bitsUm;

# Encher a máscara com 32 bits iguais a 1
$mask = 0xFFFFFFFF;

# Inserir os bits zero no inicio da máscara (bits menos significativos)
$mask = $mask << $bitsZero;

# Converte um número para o formato IP decimal
$maskDecimal = long2ip($mask);

echo "$maskDecimal";
