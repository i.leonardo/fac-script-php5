<?php

# Type = NULL
$var;
echo gettype($var) . PHP_EOL;

# Type = integer
$var = 2;
echo gettype($var) . PHP_EOL;

# Type = double
$var = 5.678;
echo gettype($var) . PHP_EOL;

# Type = string
$var = "Rafael";
echo gettype($var) . PHP_EOL;

# Type = boolean
$var = true;
echo gettype($var) . PHP_EOL;
