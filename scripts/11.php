<?php

$host = 1;
echo "Digite um IP para o ping: ";
$ip = fgets(STDIN);

do {
    $resultado = exec("ping -n 1 $ip"); # Obs.: pode usar shell_exec

    if (substr_count($resultado, "perda") > 0) {
        echo $ip . " - FALHOU!" . PHP_EOL;
    } else {
        echo $ip . " - OK!" . PHP_EOL;
    }

    $host++;
} while ($host <= 5);
