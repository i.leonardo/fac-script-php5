<?php

$link = mysql_connect("localhost", "root", "root");
if (!$link) {
    echo ("Não foi possível conectar: " . mysql_error());
    exit;
}
echo ("Conectado ...!\n");

echo ("-------------------------------------------------------------------- \n");
echo ("--- Criando Banco de Dados... \n");

$sql = ("CREATE DATABASE LPE"); // Criando Banco chamado "LPE"
if (!mysql_query($sql, $link)) {
    echo ("-- Erro ao criar banco de dados: " . mysql_error() . "\n");
    mysql_close();
    exit;
} else {
    echo ("-- Banco de Dados Criado! Nome: LPE \n");
    echo ("- Processando... \n");
}

echo ("-------------------------------------------------------------------- \n");
echo ("--- Criando a Tabela... \n");

$acesso = mysql_select_db("LPE");

if (!$acesso) {
    echo ("Não foi possível conectar: " . mysql_error());
    mysql_close();
    exit;
}

echo ("- Processando... \n");
echo ("- Banco Acessado! \n");
echo ("--- \n");

$tabela = mysql_query("CREATE TABLE Notas (
            id INT AUTO_INCREMENT,
        Materia VARCHAR(40),
        Nome VARCHAR(40),
        Nota_A INT,
            Nota_B INT,
            NT INT,
            Faltas INT,

        PRIMARY KEY(id)
        )"); //Data DATE

if (!$tabela) {
    echo ("-- Erro ao criar a Tabela no Banco de Dados: " . mysql_error() . "\n");
    mysql_close();
    exit;
} else {
    echo ("- Tabela Criado com sucesso! \n");
    echo ("--- \n");
    echo ("- Processando... \n");
}

echo ("-------------------------------------------------------------------- \n");
echo ("--- Inserindo Valores... \n");

date_default_timezone_set("America/Belem");
$date = date("Y-m-d");
echo ("Criando os Valosres na Data: $date \n");

$strSQL = "INSERT INTO Notas(";

$strSQL = $strSQL . "Materia, ";
$strSQL = $strSQL . "Nome, ";
$strSQL = $strSQL . "Nota_A, ";
$strSQL = $strSQL . "Nota_B, ";
$strSQL = $strSQL . "NT, ";
$strSQL = $strSQL . "Faltas) ";
//$strSQL = $strSQL . "Data) ";

$strSQL = $strSQL . "VALUES (";

$strSQL = $strSQL . "'PHP', ";
$strSQL = $strSQL . "'SENAI', ";
$strSQL = $strSQL . "8, ";
$strSQL = $strSQL . "6, ";
$strSQL = $strSQL . "10, ";
$strSQL = $strSQL . "2)"; //$strSQL = $strSQL . "2, ";
//$strSQL = $strSQL . "$date)";

if (!mysql_query($strSQL)) {
    ("-- Erro ao Inserir Valores na Tabela Notas: " . mysql_error() . "\n");
    mysql_close();
    exit;
} else {
    echo ("- Dados Inseridos! \n");
    echo ("- Processando... \n");
    echo ("- Fechando Conexão... \n");
    mysql_close();
}
echo ("-------------------------------------------------------------------- \n");
echo ("--- Consultando o Banco de Dados... \n");

$conexao = mysql_connect("localhost", "root", "root"); // Conexão com o Banco de Dados
if (!$conexao) {
    echo ("Não foi possível conectar: " . mysql_error());
    exit;
}
echo ("Conectado! ...\n");

// Seleciona o Banco de Dados
mysql_select_db("LPE");

//query SQL
$querySQL = "SELECT * FROM Notas";

// Executa a query (o recordset $rs contém o resultado da query)
$consulta = mysql_query($querySQL, $conexao);

while ($registro = mysql_fetch_array($consulta)) {

    echo "ID = " . $registro["id"] . ", Materia = " . $registro["Materia"] . ", Nome = " . $registro["Nome"] .
        ", N1 = " . $registro["Nota_A"] . ", N2 = " . $registro["Nota_B"] . ", NT = " . $registro["NT"] . ", Faltas = " . $registro["Faltas"] . "\n";

    $media = ($registro["Nota_A"] + $registro["Nota_B"] + $registro["NT"]) / 3;

    if ($media >= 6 && $registro["Faltas"] <= 25) {
        echo ("Media: " . $media . "\n");
        echo ("- Aluno: " . $registro["Nome"] . " \n");
        echo ("- Aprovado \n");
    } elseif ($media <= 6 || $registro["Faltas"] > 25) {
        echo ("Media: " . $media . "\n");
        echo ("Falta(s): " . $registro["Faltas"] . "\n");
        echo ("- Aluno: " . $registro["Nome"] . " \n");
        echo ("- Reprovado \n");
    } else {
        echo ("- Aluno: " . $registro["Nome"] . ", Não tem Nota Registrada! \n");
    }
    echo ("------------------------------ \n");
}
